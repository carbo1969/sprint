"use strict";

//import { KeyObject } from "crypto";
import { Response, Request, NextFunction } from "express";
import { sprintModel } from "../models/sprintModel";
var ObjectId = require('mongodb').ObjectId;


/**
 * GET /api
 * List of API examples.
 */



export class SprintController  {
    constructor(
    ) {
        console.log("construction");
    }
  
    /**
     * Start a new GDF process instance and return its associated Camunda Process ID
     * @param req The request
     * @param res The response
     */
    /*public async getApi(req: Request, res: Response): Promise<void> {
        const result = await SprintModel.get();
        res.json({ result });
    }*/

    public async getSprintsByName(req: Request, res: Response): Promise<void>{
        const result = await sprintModel.findSprintsByName(req.params.name);
        res.json({result});
    }

    public async insertNewSprint(req: Request, res: Response): Promise<void>{
        const newSprint = {
            sprintType : ObjectId(req.body.sprintType),
            progress: Number(req.body.progress),
            description: req.body.description,
            notify: Boolean(req.body.notify),
            user: req.body.user,
            createdAt: new Date(req.body.createdAt),
            startedAt: new Date(req.body.startedAt),
            finishedAt: new Date(req.body.finishedAt)
        }
        res.json(newSprint);
        const result = await sprintModel.createSprint(newSprint);
       // res.json({result});
    }
  }
  


  export const sprintController = new SprintController( );