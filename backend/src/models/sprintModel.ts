import {dbClient} from  "../util/dbClient";


export class SprintModel {

   /* async get() {
        try {
            await dbClient.mongoClient.connect();

            const database = dbClient.mongoClient.db("SprintApp");
            const usersCollection = database.collection("users");

            const query = { age: 25 };
            const options = { projection: { _id: 0, name: 1 } };

            const users = await usersCollection.findOne(query, options);

            console.log(users);
            return users;
        } catch (err) {
                console.error(err);
        } finally {
            await dbClient.mongoClient.close();
        }
    }*/

    async findSprintsByName(userName: string){
        //search_id = ObjectId(id);
        try{
            await dbClient.mongoClient.connect();

            const database = dbClient.mongoClient.db("sprint");
            const usersCollection = database.collection("sprints");

            const cursor = await usersCollection.find({ user : userName});

            const result = await cursor.toArray();
            return result;
           /* if(result.length > 0){
                result.forEach(result => {
                    console.log(result.user);
                    console.log(result.description)
                });
                console.log("Find a Sprint type with the id of: " + userName);
                console.log(result);
            }else{
                console.log("No sprint type found with the id of: " + userName);
            }*/
        } catch (err) {
            console.error(err);
        } finally {
           // await dbClient.mongoClient.close();
        }
            
    }

    async createSprint(newSprint: any) {
        await dbClient.mongoClient.connect();

        const database = dbClient.mongoClient.db("sprint");
        const usersCollection = database.collection("sprints");
        const result = await usersCollection.insertOne(newSprint);
        return result;
        
    }
}

export const sprintModel = new SprintModel( );