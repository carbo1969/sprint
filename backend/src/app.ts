import express from "express";
import cors from "cors";
import compression from "compression";  // compresses requests
import session from "express-session";
import bodyParser from "body-parser";
import mongo from "connect-mongo";
import path from "path";
import bluebird from "bluebird";
import { MONGODB_URI, SESSION_SECRET } from "./util/secrets";

import { dbClient } from  "./util/dbClient";


const MongoStore = mongo(session);

// Controllers (route handlers)
import { apiController } from "./controllers/apiController";
import { sprintController } from "./controllers/sprintController";


// API keys and Passport configuration
// import * as passportConfig from "./config/passport";


dbClient.init();


// Create Express server
const app = express();

// Express configuration
app.set("port", process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());


/**
 * API examples routes.
 */
app.get("/api", apiController.getApi);
app.get("/api/sprints/:name", sprintController.getSprintsByName);
app.post("/api/sprints/newsprint", sprintController.insertNewSprint);



export default app;
