import { Component, OnInit } from '@angular/core';
import { SprintsService} from '../sprints.service';


@Component({
  selector: 'app-sprints',
  templateUrl: './sprints.component.html',
  styleUrls: ['./sprints.component.css']
})
export class SprintsComponent implements OnInit {
  sprints:any;

  constructor(private sprintsService:SprintsService) { }

  ngOnInit(): void {
    this.getSprintsData();
  }

  getSprintsData(){
    this.sprintsService.getSprintsByName().subscribe(res => {
      console.log(res);
      this.sprints = res;
    });
  }

}
