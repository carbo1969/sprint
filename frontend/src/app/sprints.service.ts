import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class SprintsService {

  constructor(private httpClient:HttpClient) { }

  getSprintsByName(){
    return this.httpClient.get('http://localhost:3000/api/sprints/carbo');
  }
}
